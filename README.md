# single-file-tagger
This gear is designed to be able to perform tagging operations on a single files (and in the future maybe parent/child containers as well) 

#### __Note__: `DOCKER_HUB` var in `.gitlab-ci.yml` has changed to `false`. By default, this should be set to `true`

#### TODO:
 - update this gear to also be able to take parent/child containers



## Overview
Can perform a few basic actions on a file:
1. Append Tag - simply add a tag to a file
1. Remove Tag - remove a tag from a file (if present)
1. Remove And Append - removes all tags from a file and appends a tag   
1. Remove All - removes ALL tags from a file

Currently only works with dicoms...


### Summary
Performs simple tagging operations on a file.  Multiple tags can be specified by separating them with Commas.

### Cite

*{From the "cite" section of the manifest}*
*License:* *{From the "license" section of the manifest}*


### Classification
*Category:* *{From the "custom.gear-builder.category" section of the manifest}*

*Gear Level:*

- [ ] Project
- [ ] Subject
- [ ] Session
- [X] Acquisition
- [ ] Analysis

----

[[_TOC_]]

----


### Inputs

* *input_dicom*
    - **Name**: *input_dicom*
    - **Type**: *dicom*
    - **Optional**: *false*
    - **Classification**: *file*
    - **Description**: *Main input file for the Gear to perform tag operations on. must be dicom.*

  
### Config


* *Action*
    - **Name**: *Action*
    - **Type**: *string*
    - **Description**: *Set the type of action you would like to take.  Options are:
        - 'Append Tag: Add the specified tag if not already present.
        - 'Remove Tag': remove the specified tag if present.
        - 'Remove and append': Remove all existing tags and add the specified tag.
        - 'Remove all': removes all existing tags*
    - **Default**: *Append Tag*
    
* *Tag*
    - **Name**: *Tag*
    - **Type**: *string*
    - **Description**: *The specified tag(s) add or remove, comma separated
    - **Default**: *" "*

### Outputs

#### Files
None

#### Metadata
Tags added to or removed from the input file

### Pre-requisites


#### Prerequisite Gear Runs
None

#### Prerequisite Files
None

#### Prerequisite Metadata
None

## Usage

### Description
This gear uses a read only api key and the gear-toolkit metadata.write function
to modify tags using the super secret `.metadata.json` file.

#### File Specifications
This section contains specifications on any input files that the gear may need
##### *{Input-File}*
A description of the input file
    


    
### Use Cases
Tags are powerful in flywheel because they allow users to orchestrate more complicated
workflows than could be done simply by file type and classifications.  
This gear can be used to automatically assign tags to files based off of other
criteria.  

For example, all files entering a project may get a tag "PendingQA".  Once QA is done, 
a user can remove that tag, or add a "PassedQA" tag, and that can kick off the next
set of gears to run.
#### Use Case 1
***Conditions***:

 -  *{A list of conditons that result in this use case}*
 - [ ] Possibly a list of check boxes indicating things that are absent
 - [x] and things that are present

*{Description of the use case}*

### Logging

The log shows the full metadata file that's uploaded.

## FAQ
[FAQ.md](FAQ.md)

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]

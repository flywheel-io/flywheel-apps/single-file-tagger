"""Main module."""

import logging

log = logging.getLogger(__name__)

def process_tags(old_tags, new_tags, action):

    if action == "Remove All":
        return []

    if action == "Append Tag":
        return [*old_tags, *new_tags]

    if action == "Remove Tag":
        for tag in new_tags:
            if tag in old_tags:
                new_tags.remove(tag)
        return new_tags

    if action == "Remove and Append":
        return new_tags



def run(input_file, old_tags, new_tags, action, gear_context):
    """[summary]

    Returns:
        [type]: [description]
    """

    tags = process_tags(old_tags, new_tags, action)
    tags = list(set(tags))
    gear_context.metadata.update_file(input_file, tags=tags)

    return 0

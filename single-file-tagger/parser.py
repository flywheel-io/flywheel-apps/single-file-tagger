"""Parser module to parse gear config.json."""
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext


# This function mainly parses gear_context's config.json file and returns relevant inputs and options.
def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[str, str]:
    """[Summary]

    Returns:
        [type]: [description]
    """

    input_file = gear_context.get_input("input_dicom")
    old_tags = input_file["object"]["tags"][:]

    new_tags = gear_context.config['Tags']
    new_tags = new_tags.split(',')
    action = gear_context.config['Action']

    return input_file, old_tags, new_tags, action
